let
    rev = "1222e289b5014d17884a8b1c99f220c5e3df0b14";
    src = fetchTarball {
      url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
      sha256 = "1sa2m8kdak6y9183jgizg95swrv9ich05lsnli0bdc8r11wahl54";
    };
in
import src

