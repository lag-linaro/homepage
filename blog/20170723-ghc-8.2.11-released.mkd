---
author: Ben Gamari
title: "GHC 8.2.1 is available"
date: 2017-07-23
tags: release
---

= The Glasgow Haskell Compiler -- version 8.2.1 =

The GHC developers are very happy to announce the long-awaited 8.2.1
release of Glasgow Haskell Compiler. Binary and source distributions can
be found at <https://downloads.haskell.org/~ghc/8.2.1/>.

This is the second release in the 8.0 series. As such, the focus of this
release is performance, stability, and consolidation. Consequently
numerous cleanups can be seen throughout the compiler including,

 * Significant improvements in compiler performance

 * More robust support for levity polymorphism

 * Reliable DWARF debugging information

 * Improved runtime system performance on NUMA systems

 * Retooling of the cost-center profiler, including support for live
   streaming of profile data via the GHC event log

 * Interface file determinism

 * More robust treatment of join points, enabling significantly better
   code generation in many cases

 * Numerous improvements in robustness on Windows

 * and the resolution of over 500 other tickets

In addition, there are a number of new features,

 * A new, more type-safe type reflection mechanism

 * The long-awaited Backpack module system

 * Deriving strategies to disambiguate between GHC's various instance
   deriving mechanisms

 * Unboxed sum types, for efficient unpacked representation of sum data
   types

 * Compact regions, allowing better control over garbage collection
   in the presence of large heaps containing many long-lived objects.

 * Colorful messages and caret diagnostics for more legible errors

A more thorough list of the changes in this release can be found in the
[release notes](https://haskell.org/ghc/docs/8.2.1/html/users_guide/8.2.1-notes.html
).

There are a few changes in release-engineering that should be noted,

 * Binary distributions for 32-bit CentOS 6.7 have been dropped.
   Moreover, there are no dedicated CentOS 7.0 distributions as CentOS 7
   can use can use Debian 8 binaries. If you would like us to continue
   to produce 32-bit CentOS 6.7 distributions please let us know.

 * GHC HQ now builds FreeBSD and OpenBSD distributions for amd64; this
   comes after many years of these distributions being faithfully
   provided by Karel Gardas and Pali Gabor Janos, who we should heartily
   thank for their contributions.

  GHC HQ building these distributions ourselves will allow us to more
  quickly ship distributions to users by eliminating the need for a
  long lag time between source release availability and having all
  binary distributions available.

 * There is a technology-preview of an AArch64 Linux binary
   distribution, as well as an ARM Linux build. AArch64 support is quite
   preliminary but should be stable in 8.4 thanks to further linker
   fixes by Moritz Angerman. ARM should be stable.

 * GHC now tries to use the gold and lld linkers by default. These
   linkers are significantly faster than the BFD linker implementation
   that most Linux distributions use by default. If gold or lld are not
   available GHC will use the system's default linker. GHC can be forced
   to use the default linker by passing --disable-ld-override to
   configure.

This release has been the result of over a year of hard work by over 150
code contributors. Thanks to everyone who has helped in writing patches,
testing, reporting bugs, and offering feedback over the last year.

This release cycle was admittedly quite drawn out, significantly longer
than expected or desired. While we are confident that the result is
worth the wait, we have been steadily working on infrastructure which
should help shrink future release cycles and give us better testing
between releases. More details on this coming soon.

As always, let us know if you encounter trouble.

### How to get it

Both the source tarball and binary distributions for a wide variety of platforms are available [here](https://downloads.haskell.org/~ghc/8.2.1/).

## Background

Haskell is a standard lazy functional programming language.

GHC is a state-of-the-art programming suite for Haskell.  Included is
an optimising compiler generating efficient code for a variety of
platforms, together with an interactive system for convenient, quick
development.  The distribution includes space and time profiling
facilities, a large collection of libraries, and support for various
language extensions, including concurrency, exceptions, and foreign
language interfaces. GHC is distributed under a BSD-style open source license.

A wide variety of Haskell related resources (tutorials, libraries,
specifications, documentation, compilers, interpreters, references,
contact information, links to research groups) are available from the
Haskell home page (see below).

On-line GHC-related resources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Relevant URLs on the World-Wide Web:

 * [GHC home page](https://www.haskell.org/ghc/)
 * [GHC developers' home page](https://ghc.haskell.org/trac/ghc/)
 * [Haskell home page](https://www.haskell.org/)

## Supported Platforms

The list of platforms we support, and the people responsible for them,
is [here](https://ghc.haskell.org/trac/ghc/wiki/Contributors)

Ports to other platforms are possible with varying degrees of
difficulty. The [Building Guide](http://ghc.haskell.org/trac/ghc/wiki/Building) describes how to go about porting to a
new platform.

## Developers

We welcome new contributors.  Instructions on accessing our source
code repository, and getting started with hacking on GHC, are
available from the GHC's developer's site run by [Trac](http://ghc.haskell.org/trac/ghc/).
  

## Community Resources

There are mailing lists for GHC users, developers, and monitoring bug tracker
activity; to subscribe, use the Mailman
[web interface](http://mail.haskell.org/cgi-bin/mailman/listinfo).

There are several other Haskell and GHC-related mailing lists on
[haskell.org](http://www.haskell.org); for the full list, see the
[lists page](https://mail.haskell.org/cgi-bin/mailman/listinfo).

Some GHC developers hang out on the `#ghc` and `#haskell` of the Freenode IRC
network, too. See the [Haskell wiki](http://www.haskell.org/haskellwiki/IRC_channel) for details.

Please report bugs using our bug tracking system. Instructions on reporting bugs
can be found [here](http://www.haskell.org/ghc/reportabug).
